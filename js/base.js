
function calc () {
    document.getElementById("result").innerHTML = '';
    var from = document.getElementById("convertFrom").value;
    var to = document.getElementById("convertTo").value;
    var value = document.getElementById("amount").value;
    var eu = 1.95583, us = 1.72198, gb = 2.51269;
    var append = "";

    switch (from) {
        case "eu":
            value *= eu;
            break;
        case "us":
            value *= us;
            break;
        case "gb":
            value *= gb;
            break;
    }

    switch(to) {
        case "bg":
            append = " лв";
            break;
        case "eu":
            value /= eu;
            append = " ‎€";
            break;
        case "us":
            value /= us;
            append = " ‎$";
            break;
        case "gb":
            value /= gb;
            append = " ‎£";
            break;
    }
    document.getElementById("result").innerHTML = value.toFixed(4) + append;
}

// FizzBuzz function
function fizzbuzz (counter) {
    var a = [];
    for (var i = 0; i < counter; i++) {
        a.push(i+1);
    }
    for (var y = 0; y < a.length; y++) {
        if (((a[y] % 3) == 0) && ((a[y] % 5) == 0)) {
            console.log ("FizzBuzz, ");
        } else if ((a[y] % 3) == 0) {
            console.log ("Fizz, ");
        } else if ((a[y] % 5) == 0) {
            console.log ("Buzz, ");
        } else console.log(a[y] + ", ");
    }
}